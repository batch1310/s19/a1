console.log(`Hello`)

//1.
const num = 2;
const getCube = num ** 3;
console.log(`The cube of 2 is ${num} is ${getCube}`);

//2.
let address = ["258 Washington Ave NW", "California 9011"]

const getAddress = (e) => {
	const [state, city] = e;

	console.log(`I live at ${state}, ${city}`);
}
getAddress(address);

//3.
let animal = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: 1075,
	length: "20 ft 3 in"
}

let animalDetails = ({name, type, weight, length}) => {
	console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length}`)
}
animalDetails(animal);

//4.
let number = [1, 2, 3, 4, 5];

number.forEach( (e) => console.log(e));

for(i = number; i >= 4; i--){
	
}




//5.
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const newDog = new Dog("Frankie", "5", "Miniature Dachshund");
console.log(newDog);